#!/bin/sh
export MAKEFLAGS="-j$(nproc)"
_script_dir="$(pwd)"
_distr_dir="${_script_dir}/distr"
_build_dir="${_script_dir}/build"

export CXXFLAGS="-march=native -O3 -pipe -fstack-protector-strong"
#if [ "${STONEAGE}" -eq '1' ]; then
#	echo "setting cxxflags to use c++17 on old compilers..."
#	export CXXFLAGS="${CXXFLAGS} -std=c++1z -isystem $(pwd)/include_wrappers/"
#fi
mkdir -p "${_distr_dir}" "${_build_dir}"

build()
{
	mkdir -p "${_build_dir}/${1}"
	cd "${_build_dir}/${1}"

	cmake -DCMAKE_BUILD_TYPE='Release' -DCMAKE_INSTALL_PREFIX="${_distr_dir}/" -DCMAKE_PREFIX_PATH="${_distr_dir}/lib/cmake/" "${_script_dir}/${1}"
	cmake --build .
	cmake --build . --target install

	if [ -d "${_script_dir}/${1}/Common" ] ; then
		mkdir -p 'common'
		cd 'common'
		cmake -DCMAKE_INSTALL_PREFIX="${_distr_dir}/" -DCMAKE_PREFIX_PATH="${_distr_dir}/lib/cmake/" "${_script_dir}/${1}/Common"
		cmake --build .
		cmake --build . --target 'install'
		cd '../'
	fi

	cd '../'
}

build sib_utils
build Kawaii3D
build KawaiiRenderer
build MisakaRenderer
build KawaiiFigures3D
build KawaiiAssimp
build Kawaii3D-Samples

for i in $@
do
	build $i
done

